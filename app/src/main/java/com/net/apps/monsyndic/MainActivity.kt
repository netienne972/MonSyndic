package com.net.apps.monsyndic

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.setContent
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.viewinterop.viewModel
import com.net.apps.monsyndic.ui.theme.MonSyndicTheme

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MonSyndicTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    DefaultPreview()
                }
            }
        }
    }
}

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun SyndicHomeScreen() {
    val viewModel: AppViewModel = viewModel()

    AndroidView(
        modifier = Modifier.fillMaxSize(),
        viewBlock = { WebView(it) }
//        viewBlock = ::WebView
    ) { webView ->
        with(webView) {
            settings.javaScriptEnabled = true
            webViewClient = WebViewClient()
            loadUrl(viewModel.extranetUrl)
        }
    }
}

@Preview(
    showSystemUi = true,
    showBackground = true,
    name = "Syndic home screen"
)
@Composable
fun DefaultPreview() {
    Column(
//        modifier = Modifier.padding(8.dp)
    ) {
        SyndicHomeScreen()
    }
}